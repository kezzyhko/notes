from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from notes.models import NoteModel, NoteForm
from django.contrib.auth.views import LoginView


def index(request):
	if request.user.is_authenticated:
		user_notes = NoteModel.objects.filter(author=request.user).order_by('-creation_date')
	else:
		user_notes = None
	return render(request, 'index.html', {'notes': user_notes})


class MyLoginView(LoginView):
    template_name = 'login.html'


# https://simpleisbetterthancomplex.com/tutorial/2017/02/18/how-to-create-user-sign-up-view.html
def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			login(request, user)
			return redirect('index')
	else:
		form = UserCreationForm()
	return render(request, 'form.html', {'form': form, 'title': "Sign Up", 'button_text': "Sign Up"})


@login_required()
def view(request, id):
	user_notes = NoteModel.objects.filter(author=request.user)
	note = get_object_or_404(user_notes, id=id)
	return render(request, 'view.html', {'note': note})



@login_required()
def create(request):
	if request.method == 'POST':
		form = NoteForm(request.POST)
		if form.is_valid():
			form.instance.author = request.user
			note_data = form.save()
			return redirect('view', id=note_data.id)
	else:
		form = NoteForm()
	return render(request, 'form.html', {'form': form, 'title': "Create note", 'button_text': "Create"})


@login_required()
def edit(request, id):
	user_notes = NoteModel.objects.filter(author=request.user)
	note = get_object_or_404(user_notes, id=id)
	if request.method == 'POST':
		form = NoteForm(request.POST, instance=note)
		if form.is_valid():
			form.save()
			return redirect('view', id=id)
	else:
		form = NoteForm(instance=note)
	return render(request, 'form.html', {'form': form, 'title': "Edit note", 'button_text': "Save"})


@login_required()
def delete(request, id):
	user_notes = NoteModel.objects.filter(author=request.user)
	note = get_object_or_404(user_notes, id=id)
	note.delete()
	return redirect('index')
