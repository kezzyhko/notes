from django.http.response import Http404
from django.test import LiveServerTestCase, TestCase, Client
from django.test.client import RequestFactory
from django.contrib.auth.models import AnonymousUser, User
from django.contrib.sessions.middleware import SessionMiddleware
from django.shortcuts import reverse

from selenium.webdriver import Chrome as Webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager as DriverManager
from selenium.webdriver.common.action_chains import ActionChains

from notes.models import NoteModel
from notes import views



class WorkflowTest(LiveServerTestCase):

	def test_workflow(self):
		options = Options()
		options.add_argument('--no-sandbox')
		options.add_argument('--headless')
		webdriver = Webdriver(DriverManager().install(), options=options)
		webdriver.get(self.live_server_url)

		# sign up
		webdriver.find_element_by_id('signup_link').click()
		webdriver.find_element_by_id('id_username').send_keys("Test")
		webdriver.find_element_by_id('id_password1').send_keys("QwE123@-")
		webdriver.find_element_by_id('id_password2').send_keys("QwE123@-")
		webdriver.find_element_by_id('submit_button').click()

		# create note
		webdriver.find_element_by_id('create_link').click()
		webdriver.find_element_by_id('id_title').send_keys("Title321")
		webdriver.find_element_by_id('id_text').send_keys("text123text")
		webdriver.find_element_by_id('submit_button').click()
		self.assertTrue("<h1>Title321</h1>" in webdriver.page_source)
		self.assertTrue("text123text" in webdriver.page_source)

		# edit note
		webdriver.find_element_by_id('edit_link').click()
		title_field = webdriver.find_element_by_id('id_title')
		title_field.clear()
		title_field.send_keys("Another Title")
		webdriver.find_element_by_id('id_text').send_keys(" More text ...")
		webdriver.find_element_by_id('submit_button').click()
		self.assertTrue("<h1>Another Title</h1>" in webdriver.page_source)
		self.assertTrue("text123text More text ..." in webdriver.page_source)

		# access note from main page
		webdriver.get(self.live_server_url)
		note_links = webdriver.find_elements_by_class_name('note_link')
		self.assertEqual(len(note_links), 1)
		note_link = note_links[0]
		self.assertEqual(note_link.get_attribute('innerText'), "Another Title")
		
		note_link.click()
		edit_link = webdriver.find_element_by_id('edit_link')
		delete_link = webdriver.find_element_by_id('delete_link')

		# click delete and then do not confirm
		delete_link.click()
		self.assertEqual(delete_link.get_attribute('innerText'), "Click again to delete")
		ActionChains(webdriver).move_to_element(edit_link).perform()
		self.assertEqual(delete_link.get_attribute('innerText'), "Delete")

		# delete note
		delete_link.click()
		delete_link.click()
		webdriver.get(self.live_server_url)
		note_links = webdriver.find_elements_by_class_name('note_link')
		self.assertEqual(len(note_links), 0)
		self.assertTrue("Another Title" not in webdriver.page_source)



class IndexTest(TestCase):

	def setUp(self):
		factory = RequestFactory()
		self.request = factory.get(reverse('index'))

	def test_index_unauthorized(self):
		self.request.user = AnonymousUser()
		response = views.index(self.request)
		self.assertContains(response, reverse('signup'))
		self.assertContains(response, reverse('login'))
		self.assertNotContains(response, reverse('logout'))

	def test_index_authorized(self):
		self.request.user = User.objects.create_user(username='testusername', password='testpassword')
		response = views.index(self.request)
		self.assertNotContains(response, reverse('signup'))
		self.assertNotContains(response, reverse('login'))
		self.assertContains(response, reverse('logout'))



class SignupTest(TestCase):

	def setUp(self):
		self.factory = RequestFactory()

	def test_get(self):
		request = self.factory.get(reverse('signup'))
		response = views.signup(request)
		self.assertContains(response, '<form method="post">',)
		self.assertContains(response, '<button type="submit" id="submit_button">')

	def test_valid_post(self):
		data = {'username': 'Test', 'password1': 'QwE123@-', 'password2': 'QwE123@-'}
		request = self.factory.post(reverse('signup'), data)
		SessionMiddleware(request).process_request(request)
		response = views.signup(request)
		response.client = Client()
		self.assertRedirects(response, reverse('index'))
		self.assertTrue(User.objects.filter(username='Test').exists())

	def test_invalid_post_1(self):
		data = {'username': '~', 'password1': '~', 'password2': '~'}
		request = self.factory.post(reverse('signup'), data)
		response = views.signup(request)
		self.assertContains(response, 'Enter a valid username')
		self.assertContains(response, 'The password is too similar to the username')
		self.assertContains(response, 'This password is too short')

	def test_invalid_post_2(self):
		data = {'username': '~', 'password1': '~'}
		request = self.factory.post(reverse('signup'), data)
		response = views.signup(request)
		self.assertContains(response, 'This field is required')

	def test_invalid_post_3(self):
		data = {'username': '~', 'password1': '~', 'password2': '123'}
		request = self.factory.post(reverse('signup'), data)
		response = views.signup(request)
		self.assertContains(response, 'The two password fields didn’t match')



class ViewTest(TestCase):

	def setUp(self):
		self.user1 = User.objects.create_user(username='test1', password='password1')
		self.user2 = User.objects.create_user(username='test2', password='password2')
		self.note = NoteModel.objects.create(title="Title", text="Text123Text", author=self.user1)
		self.note.save()
		factory = RequestFactory()
		self.request = factory.get(reverse('view', kwargs={'id': self.note.id}))

	def test_correct_display(self):
		self.request.user = self.user1
		response = views.view(self.request, id=self.note.id)
		self.assertContains(response, "<h1>Title</h1>", html=True)
		self.assertContains(response, "Text123Text")

	def test_can_view_own(self):
		self.request.user = self.user1
		response = views.view(self.request, id=self.note.id)
		self.assertEqual(response.status_code, 200)

	def test_cannot_view_other(self):
		try:
			self.request.user = self.user2
			response = views.view(self.request, id=self.note.id)
		except Http404:
			pass
		else:
			self.assertEqual(response.status_code, 404)



class EditTest(TestCase):

	def setUp(self):
		self.user1 = User.objects.create_user(username='test1', password='password1')
		self.user2 = User.objects.create_user(username='test2', password='password2')
		self.note = NoteModel.objects.create(title="Title", text="Text123Text", author=self.user1)
		self.note.save()
		self.path = reverse('edit', kwargs={'id': self.note.id})
		self.factory = RequestFactory()

	def test_cannot_edit_other(self):
		try:
			request = self.factory.get(self.path)
			request.user = self.user2
			response = views.edit(request, id=self.note.id)
		except Http404:
			pass
		else:
			self.assertEqual(response.status_code, 404)

	def test_cannot_empty(self):
		data = {'title': '', 'text': ''}
		request = self.factory.post(self.path, data)
		request.user = self.user1
		response = views.edit(request, id=self.note.id)
		self.assertContains(response, "This field is required.")

	def test_data_save(self):
		data = {'title': 'NewTitle', 'text': 'New text...'}
		request = self.factory.post(self.path, data)
		request.user = self.user1
		response = views.edit(request, id=self.note.id)
		updated_note = NoteModel.objects.get(id=self.note.id)
		for key, expected_value in data.items():
			real_value = getattr(updated_note, key)
			self.assertEqual(real_value, expected_value)
