from django.db import models
from django.forms import ModelForm, Textarea
from django.conf import settings
import uuid


class NoteModel(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	creation_date = models.DateTimeField(auto_now_add=True)
	title = models.CharField(max_length=50)
	text = models.CharField(max_length=1000)
	author = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
	)

	def __str__(self):
		return self.title


class NoteForm(ModelForm):
	class Meta:
		model = NoteModel
		fields = ['title', 'text']
		widgets = {
			'text': Textarea(attrs={'cols': 80, 'rows': 20}),
		}